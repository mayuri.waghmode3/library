# library

Deploying simple springboot project using web thymeleaf to AWS ec2 instance

Running simple spring boot app on EC2 instance
# We need to do another configuration in pom.xml to make the jar executable by adding this section in pom.xml
    <build>
    	<plugins>
    		<plugin>
    			<groupId>org.springframework.boot</groupId>
    			<artifactId>spring-boot-maven-plugin</artifactId>
    			<configuration>
    				<executable>true</executable>
    			</configuration>
    		</plugin>
    	</plugins>
    </build>
# first build the artifact in local machine by maven command
	mvn clean install
	to build artifact : File -> Project Structure -> Project Settings -> Artifacts -> Click green plus sign -> Jar -> From modules with dependencies... Press apply and OK.
	run jar file locally : java -jar library-1.0-SNAPSHOT.jar
	http://localhost:8080
	http://localhost:8080/greeting?name=mayuri
# Now we need to create one bucket called s3-spring-library-bucket in AWS S3 and upload the jar file generated in the target directory of the project
    Region of my s3 bucket with public access is  --region=us-east-1
#   Create an s3 role with full access

# Launch an ec2 instance - > 
	first select the IAM role that we have created earlier, in my case I named that role as AmazonS3FullAccess. You can attach it later also once u launch the instance.
	Then we need to paste the USER DATA in the Advance Details section. You can copy the whole code as motioned above/attached code base. Or u can execute it later.
	Now choose/edit the security group to allow HTTP traffic to port 80
	Finally choose one key pair and finish the instance creation step by clicking Launch Instance button


# script to run on ec2 instance  or can paste as  the USER DATA in the Advance Details section while launcing ec2 instance

#!/bin/bash
# install updates
yum update -y

# install apache httpd
yum install httpd -y

# install java 8
yum install java-1.8.0 -y

# remove java 1.7
yum remove java-1.7.0-openjdk -y

# create the working directory
mkdir /opt/spring-boot-ec2-demo

# create configuration specifying the used profile
echo "RUN_ARGS=--spring.profiles.active=ec2" > /opt/spring-boot-ec2-demo/spring-boot-ec2-demo.conf

# download the maven artifact from S3
aws s3 cp s3://s3-spring-library-bucket/library-1.0-SNAPSHOT.jar /opt/spring-boot-ec2-demo/ --region=us-east-1

# springboot login shell disabled
# create a springboot user to run the app as a service
useradd springboot


# springboot login shell disabled
chown springboot:springboot /opt/spring-boot-ec2-demo/library-1.0-SNAPSHOT.jar
chmod 500 /opt/spring-boot-ec2-demo/library-1.0-SNAPSHOT.jar
ln -s /opt/spring-boot-ec2-demo/library-1.0-SNAPSHOT.jar /etc/init.d/spring-boot-ec2-demo

# forward port 80 to 8080
echo "<VirtualHost *:80>
  ProxyRequests Off
  ProxyPass / http://localhost:8080/
  ProxyPassReverse / http://localhost:8080/
</VirtualHost>" >> /etc/httpd/conf/httpd.conf

# start the httpd and spring-boot-ec2-demo
service httpd start
service spring-boot-ec2-demo start

# automatically start httpd and spring-boot-ec2-demo if this ec2 instance reboots
chkconfig httpd on
chkconfig spring-boot-ec2-demo on

# finally
	Ec2 instance : java -jar library-1.0-SNAPSHOT.jar
	Browser : http://ec2-3-92-24-223.compute-1.amazonaws.com/greeting?name=Mayuri

