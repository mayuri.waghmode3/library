package com.max.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class GreetingController {


    // /greeting?name=mayuri
    @GetMapping("/greeting")
    public String greet(@RequestParam(name = "name",required = false, defaultValue = "World")
                                       String name, Model model){

        model.addAttribute("name", name);
        return "greeting";
    }
}
